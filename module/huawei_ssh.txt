{%- if ssh is defined %}
stelnet server enable
ssh client first-time enable
aaa
 local-user {{ssh.username}} password ir {{ssh.password}}
 local-user {{ssh.username}} service-type http ssh telnet terminal
 local-user {{ssh.username}} privilege level 15
 quit

user-interface vty 0 4
 authentication-mode aaa
 protocol inbound all
 quit
ssh authentication-type default password
ssh user {{ssh.username}}
ssh user {{ssh.username}} authentication-type password
ssh user {{ssh.username}} service-type stelnet
{% endif %}
