{%- if telnet is defined %}
telnet server enable	
user-interface vty 0 4	
 authentication-mode scheme	
 quit	
local-user {{telnet.username}}	
 password simple {{telnet.password}}
 service-type telnet ssh
 authorization-attribute user-role network-admin	
 authorization-attribute user-role network-operator	
 quit	
{%- endif %}
