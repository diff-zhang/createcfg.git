# createcfg

#### 介绍
基于yaml，jinja的配置生成工具

#### 软件架构
Python+jinja+yaml


#### 安装教程

1.  安装Python
2.  安装jinja
3.  引用yaml

#### 使用说明

Createcfg.py yaml参数文件 模块 文件输出方式 输出文件夹名称
1.  yaml参数文件，参考cfg_yaml，可以从cfg_yaml文件夹里面拷贝文件更改后使用
2.  模块，可自己编写，或者diff张更新
3.  文件输出方式，a为追加，适合不同的功能配置，写到一个输出文件里面，w为输出为全新的文件。
4.  输出文件夹名称，默认为当前文件夹，如果生成的是批量配置，建议填写这个参数。
5.  你可以参考module文件夹里面的模块，编写自己的模块

#### 使用举例

1.  为了使用方便，进入“cfg_yaml”文件夹，再进入h3c_dhcp文件夹，复制template.yaml到主文件夹。可以修改文件名称为test.yaml
2.  打开test.html,修改相关参数，比如修改dhcp相关的参数并保存
3.  命令行执行Createcfg.py test.yaml none w
4.  上面test.yaml为生成配置文件的参数文件，none指通过test.yaml文件里的modules指定模块，w为覆盖相同文件名的文件。
5.  运行上述命令即可生成配置文件，配置文件以test.yaml里面host参数命名。

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  Gitee 官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解 Gitee 上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
5.  Gitee 官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
